# Installation and running

Clone from git

```sh
$ git clone git@gitlab.com:orest/microblog.git
$ cd microblog
```

Create and activate virtual environment

```sh
$ python3 -m venv venv
$ source venv/bin/activate  
```

Install necessary packages from requirements.txt

```sh
(venv) $ pip install -r requirements.txt
```

Run your app

```sh
(venv) $ flask run
```